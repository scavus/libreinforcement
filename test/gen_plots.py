#!/usr/bin/env python

import sys
from copy import deepcopy
import numpy as np
import rl_algorithm as rl
import rl_utility as rlut
from env_gridworld import *

q_table_list = []
stats_list = []

algorithms = {'mc': [rl.train_mc_first_visit, rl.SettingsMc()],
	'ql': [rl.train_q_learning, rl.SettingsQLearning()],
	'sarsa': [rl.train_sarsa, rl.SettingsSarsa()],
	'ql-et': [rl.train_q_learning_et, rl.SettingsQlearningEt()],
	'dq': [rl.train_dyna_q, rl.SettingsDynaQ()],
	'ps': [rl.train_prioritized_sweeping, rl.SettingsPrioritizedSweeping()]}

algorithms['mc'][1].max_ep_count = 500
algorithms['ql'][1].max_ep_count = 500
algorithms['sarsa'][1].max_ep_count = 500
algorithms['ql-et'][1].max_ep_count = 500
algorithms['dq'][1].max_ep_count = 500
algorithms['ps'][1].max_ep_count = 500

envs = {'ENV1': Gridworld(10, Point2d(4, 0), Point2d(8, 8), portal_enable=False, wind_enable=False),
	'ENV2': Gridworld(10, Point2d(4, 0), Point2d(8, 8), portal_enable=True, wind_enable=True)}

envs['ENV2'].portal = (Point2d(2, 0), Point2d(4, 5))
envs['ENV2'].wind_row = 4
envs['ENV2'].wind_dir = WIND_DIR_L
envs['ENV2'].wind_prob = 0.8

for env_name, env in envs.items():
	for algorithm_name, algorithm in algorithms.items():
		np.random.seed(0)
		algorithm_fn = algorithm[0]
		settings = deepcopy(algorithm[1])
		q_table, stats = algorithm_fn(env, settings)
		rlut.statistics_plot(stats, env, env_name, algorithm_name)
		q_table_list.append(q_table)
		stats_list.append(stats)

	rlut.statistics_plot_n(stats_list, env, env_name, algorithms.keys())
	q_table_list = []
	stats_list = []
