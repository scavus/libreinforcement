import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()

plt.style.use('classic')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

def moving_avg(interval, window_size):
	window = np.ones(window_size) / window_size
	return np.convolve(np.pad(interval, window_size, 'edge'), window, 'valid')

def statistics_plot(stats, env, env_name, algorithm_name):
	window_size = int(stats.max_ep_count / 4)
	step_data = stats.steps_per_episode
	step_data = moving_avg(step_data, window_size)
	reward_data = stats.total_reward_per_episode / stats.steps_per_episode
	reward_data = moving_avg(reward_data, window_size)

	plt.figure()
	plt.plot(step_data, label=algorithm_name)
	plt.xlim(0, stats.max_ep_count)
	plt.title('Number of steps per episode for ' + env_name)
	plt.xlabel('Episodes')
	plt.ylabel('Steps')
	plt.legend()
	plt.savefig('plots/' + env_name.lower() + '-' + algorithm_name.lower() + '-steps.pdf')
	plt.close()

	plt.figure()
	plt.plot(reward_data, label=algorithm_name)
	plt.xlim(0, stats.max_ep_count)
	plt.title('Expected reward per episode for ' + env_name)
	plt.xlabel('Episodes')
	plt.ylabel('Expected Reward')
	plt.legend()
	plt.savefig('plots/' + env_name.lower() + '-' + algorithm_name.lower() + '-rewards.pdf')
	plt.close()

	plt.figure()
	visits_per_state = np.reshape(stats.visits_per_state, (env.dim, env.dim))
	ax = sns.heatmap(visits_per_state, linewidths=1.0)
	sns.plt.title('Heatmap showing number of visits per state for ' + env_name + ' using ' + algorithm_name)
	fig = ax.get_figure()
	fig.savefig('plots/' + env_name.lower() + '-' + algorithm_name.lower() + '-heatmap.pdf')
	plt.close()

def statistics_plot_n(stats_list, env, env_name, algorithm_names):
	max_ep_count = stats_list[0].max_ep_count
	window_size = int(max_ep_count / 4)

	plt.figure()
	for i, algorithm_name in enumerate(algorithm_names):
		step_data = stats_list[i].steps_per_episode
		step_data = moving_avg(step_data, window_size)
		plt.plot(step_data, label=algorithm_name)
	plt.xlim(0, max_ep_count)
	plt.title('Number of steps per episode for ' + env_name)
	plt.xlabel('Episodes')
	plt.ylabel('Steps')
	plt.legend()
	plt.savefig('plots/' + env_name.lower() + '-' + 'all' + '-steps.pdf')
	plt.close()

	plt.figure()
	for i, algorithm_name in enumerate(algorithm_names):
		reward_data = stats_list[i].total_reward_per_episode / stats_list[i].steps_per_episode
		reward_data = moving_avg(reward_data, window_size)
		plt.plot(reward_data, label=algorithm_name)
	plt.xlim(0, max_ep_count)
	plt.title('Expected reward per episode for ' + env_name)
	plt.xlabel('Episodes')
	plt.ylabel('Expected Reward')
	plt.legend()
	plt.savefig('plots/' + env_name.lower() + '-' + 'all' + '-rewards.pdf')
	plt.close()

	return
