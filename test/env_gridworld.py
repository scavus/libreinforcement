
from copy import deepcopy
import numpy as np

class Point2d:
	def __init__(self, x, y):
		self.x = x
		self.y = y

ACTION_MOVE_R = 0
ACTION_MOVE_L = 1
ACTION_MOVE_U = 2
ACTION_MOVE_D = 3
ACTION_TELEPORT = 4

WIND_DIR_R = 0
WIND_DIR_L = 1
WIND_DIR_U = 2
WIND_DIR_D = 3

class Gridworld:
	def __init__(self, n, s, g, portal_enable=False, wind_enable=False):
		self.dim = n
		self.start_pos = s
		self.goal_pos = g
		self.agent_pos = deepcopy(self.start_pos)
		self.portal_enable = portal_enable
		self.portal = (None, None)
		self.wind_enable = wind_enable
		self.wind_row = None
		self.wind_dir = None
		self.wind_prob = 0

		self.state_count = n * n
		if self.portal_enable == True:
			self.action_count = 5
		else:
			self.action_count = 4

	def reset(self):
		self.agent_pos = deepcopy(self.start_pos)

	def pos_to_state(self, pos):
		return self.dim * pos.y + pos.x

	def state_to_pos(self, state):
		return Point2d(state % self.dim, state // self.dim)

	def get_start_state(self):
		return self.pos_to_state(self.start_pos)

	def get_goal_state(self):
		return self.pos_to_state(self.goal_pos)

	def get_agent_state(self):
		return self.pos_to_state(self.agent_pos)

	def sample_initial_state(self):
		self.agent_pos.x = np.random.choice(self.dim)
		self.agent_pos.y = np.random.choice(self.dim)
		return self.pos_to_state(self.agent_pos)

	def check_bounds(self, pos):
		if pos.x >= self.dim or pos.x < 0 or pos.y >= self.dim or pos.y < 0:
			return False
		else:
			return True

	def move(self, action):
		transition_available = True

		if self.wind_enable == True and self.agent_pos.y == self.wind_row:
			agent_pos_next = deepcopy(self.agent_pos)
			wind_blow = np.random.choice([True, False], p=[self.wind_prob, 1.0 - self.wind_prob])
			if wind_blow == True:
				if self.wind_dir == WIND_DIR_R:
					agent_pos_next.x += 1
				elif self.wind_dir == WIND_DIR_L:
					agent_pos_next.x -= 1
				elif self.wind_dir == WIND_DIR_U:
					agent_pos_next.y -= 1
				elif self.wind_dir == WIND_DIR_D:
					agent_pos_next.y += 1

			transition_available = self.check_bounds(agent_pos_next)
			if transition_available == True:
				self.agent_pos = deepcopy(agent_pos_next)

		agent_pos_next = deepcopy(self.agent_pos)
		if action != ACTION_TELEPORT:
			if action == ACTION_MOVE_R:
				agent_pos_next.x += 1
			elif action == ACTION_MOVE_L:
				agent_pos_next.x -= 1
			elif action == ACTION_MOVE_U:
				agent_pos_next.y -= 1
			elif action == ACTION_MOVE_D:
				agent_pos_next.y += 1

			transition_available = self.check_bounds(agent_pos_next)
		else:
			if self.pos_to_state(self.agent_pos) == self.pos_to_state(self.portal[0]):
				agent_pos_next = deepcopy(self.portal[1])
				transition_available = self.check_bounds(agent_pos_next)
			elif self.pos_to_state(self.agent_pos) == self.pos_to_state(self.portal[1]):
				agent_pos_next = deepcopy(self.portal[0])
				transition_available = self.check_bounds(agent_pos_next)
			else:
				transition_available = False

		if transition_available == True:
			self.agent_pos = deepcopy(agent_pos_next)

		return transition_available

	def step(self, action):
		terminate = False

		transition_available = self.move(action)

		if transition_available == True:
			reward = -5
		else:
			reward = -1000

		if self.pos_to_state(self.agent_pos) == self.pos_to_state(self.goal_pos):
			reward = 100
			terminate = True

		return reward, self.pos_to_state(self.agent_pos), terminate


class ApproximatorRbfFn:
	def __init__(self):
		self.feature_count = 2
		self.weights = np.ones(self.feature_count)
		self.feature_fns = [self.feature_0, self.feature_1, self.feature_2]

	def __call__(self, env, s, a=None):
		if a == None:
			q = np.zeros(env.action_count)
			for a in range(env.action_count):
				for i in range(self.feature_count):
					q[a] += self.weights[i] * self.feature_fns[i](env, s, a)
			return q
		else:
			q = 0
			for i in range(self.feature_count):
				q += self.weights[i] * self.feature_fns[i](env, s, a)
			return q

	def feature_0(self, env, s, a):
		return 1

	def feature_2(self, env, s, a):
		agent_pos = env.state_to_pos(s)

		agent_pos_next = deepcopy(agent_pos)
		if a != ACTION_TELEPORT:
			if a == ACTION_MOVE_R:
				agent_pos_next.x += 1
			elif a == ACTION_MOVE_L:
				agent_pos_next.x -= 1
			elif a == ACTION_MOVE_U:
				agent_pos_next.y -= 1
			elif a == ACTION_MOVE_D:
				agent_pos_next.y += 1

			transition_available = env.check_bounds(agent_pos_next)
		else:
			if env.pos_to_state(agent_pos) == env.pos_to_state(env.portal[0]):
				agent_pos_next = deepcopy(env.portal[1])
				transition_available = env.check_bounds(agent_pos_next)
			elif env.pos_to_state(env.agent_pos) == env.pos_to_state(env.portal[1]):
				agent_pos_next = deepcopy(env.portal[0])
				transition_available = env.check_bounds(agent_pos_next)
			else:
				transition_available = False

		if transition_available == True:
			agent_pos = deepcopy(agent_pos_next)

		n = env.dim - 1
		m = (env.dim - 1) / 2
		agent_pos = Point2d((agent_pos.x - m) / n, (agent_pos.y - m) / n)
		goal_pos = Point2d((env.goal_pos.x - m) / n, (env.goal_pos.y - m) / n)
		d = (agent_pos.x - goal_pos.x)**2 + (agent_pos.y - goal_pos.y)**2
		# print(d)
		return self.rbf(agent_pos, goal_pos, sigma=1.0)

	def feature_1(self, env, s, a):
		agent_pos = env.state_to_pos(s)

		agent_pos_next = deepcopy(agent_pos)
		if a != ACTION_TELEPORT:
			if a == ACTION_MOVE_R:
				agent_pos_next.x += 1
			elif a == ACTION_MOVE_L:
				agent_pos_next.x -= 1
			elif a == ACTION_MOVE_U:
				agent_pos_next.y -= 1
			elif a == ACTION_MOVE_D:
				agent_pos_next.y += 1

			transition_available = env.check_bounds(agent_pos_next)
		else:
			if env.pos_to_state(agent_pos) == env.pos_to_state(env.portal[0]):
				agent_pos_next = deepcopy(env.portal[1])
				transition_available = env.check_bounds(agent_pos_next)
			elif env.pos_to_state(env.agent_pos) == env.pos_to_state(env.portal[1]):
				agent_pos_next = deepcopy(env.portal[0])
				transition_available = env.check_bounds(agent_pos_next)
			else:
				transition_available = False

		if transition_available == True:
			agent_pos = deepcopy(agent_pos_next)
		else:
			return 0

		n = env.dim - 1
		m = (env.dim - 1) / 2
		agent_pos = Point2d((agent_pos.x - m) / n, (agent_pos.y - m) / n)
		goal_pos = Point2d((env.goal_pos.x - m) / n, (env.goal_pos.y - m) / n)
		portal0_pos = Point2d((env.portal[0].x - m) / n, (env.portal[0].y - m) / n)
		portal1_pos = Point2d((env.portal[1].x - m) / n, (env.portal[1].y - m) / n)

		sigma = 1.0
		d1 = (portal1_pos.x - goal_pos.x)**2 + (portal1_pos.y - goal_pos.y)**2
		d2 = (agent_pos.x - portal0_pos.x)**2 + (agent_pos.y - portal0_pos.y)**2
		d3 = (agent_pos.x - goal_pos.x)**2 + (agent_pos.y - goal_pos.y)**2
		g1 = np.exp(-((d1 + d2) / (2 * sigma ** 2)))
		g2 = np.exp(-((d3) / (2 * sigma ** 2)))
		return np.max([g1, g2])

	def rbf(self, s, c, sigma):
		return np.exp(-((s.x - c.x) ** 2 + (s.y - c.y) ** 2) / (2 * sigma ** 2))
