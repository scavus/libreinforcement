
# LibReinforcement

A small python library containing a subset of reinforcement learning algorithms given in Sutton and Bartos book.

## Methods
- First Visit Monte Carlo
- Q-Learning
- SARSA
- Q-Learning with Elligibility Traces
- Dyna-Q
- Prioritized Sweeping
- Q-Learning with Function Approximation
- SARSA with Function Approximation

## Dependencies
Numpy and Pyprind.

```
pip install numpy pyprind
```
