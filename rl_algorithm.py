
import heapq
import numpy as np
import pyprind


class Statistics:
	def __init__(self, max_ep_count, state_count):
		self.max_ep_count = max_ep_count
		self.steps_per_episode = np.zeros(max_ep_count, dtype=int)
		self.total_reward_per_episode = np.zeros(max_ep_count)
		self.visits_per_state = np.zeros(state_count, dtype=int)

	def reset(self):
		self.steps_per_episode.fill(0)
		self.total_reward_per_episode.fill(0)
		self.visits_per_state.fill(0)

	def update(self, ep, s, r):
		self.steps_per_episode[ep] += 1
		self.total_reward_per_episode[ep] += r
		self.visits_per_state[s] += 1


class PolicyGreedyFn:
	def __init__(self):
		return

	def __call__(self, q_table):
		return np.argmax(q_table)


class PolicyEGreedyFn:
	def __init__(self, epsilon):
		self.epsilon_initial = epsilon
		self.epsilon = epsilon

	def __call__(self, q_table):
		a = np.argmax(q_table)
		greedy = np.random.choice([False, True], p=[self.epsilon, 1.0 - self.epsilon])

		if greedy == True:
			return a
		else:
			a = np.concatenate([np.arange(a), np.arange(a + 1, len(q_table))])
			return np.random.choice(a)


class PolicySoftmaxFn:
	def __init__(self, temperature):
		self.temperature = temperature

	def __call__(self, q_table):
		q_table_sum = np.sum(np.exp(q_table / self.temperature))
		policy = np.exp(q_table / self.temperature) / q_table_sum
		return np.random.choice(len(q_table), p=policy)


# First visit MC
class SettingsMc:
	def __init__(self):
		self.max_ep_count = 1000
		self.gamma = 0.95
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)

def train_mc_first_visit(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))
	rets = np.zeros((env.state_count, env.action_count, settings.max_ep_count))
	ret_counters = np.zeros((env.state_count, env.action_count), dtype=int)

	for k in pyprind.prog_bar(range(settings.max_ep_count)):
		# Generate an episode using policy
		ep = []
		env.reset()
		s = env.get_start_state()
		terminate_ep = False
		while terminate_ep == False:
			a = settings.policy_fn(q_table[s])
			# Take action a, observe r, s'
			r, s_prime, terminate_ep = env.step(a)

			ep.append((s, a, r))
			s = s_prime

			stats.update(k, s, r)

		# For each pair s, a appearing in the episode:
		unique_visits = []
		for t, (s, a, _) in enumerate(ep):
			if (s, a) in unique_visits:
				continue
			else:
				unique_visits.append((s, a))

			ret = 0
			for t_prime, (_, _, r) in enumerate(ep[t:]):
				ret += (settings.gamma ** t_prime) * r

			ret_counter = ret_counters[s, a]
			ret_counter += 1
			rets[s, a, ret_counter] = ret
			q_table[s, a] = np.mean(rets[s, a, 0:ret_counter])
			ret_counters[s, a] = ret_counter

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return q_table, stats


# Q-learning
class SettingsQLearning:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)

def train_q_learning(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		terminate_ep = False
		while terminate_ep == False:
			# Choose a from s using policy derived from Q
			a = settings.policy_fn(q_table[s])
			# Take action a, observe r, s'
			r, s_prime, terminate_ep = env.step(a)
			q_table[s, a] += settings.alpha * (r + settings.gamma * np.max(q_table[s_prime]) - q_table[s, a])
			s = s_prime

			stats.update(ep, s, r)

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return q_table, stats


# SARSA
class SettingsSarsa:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)

def train_sarsa(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		# Choose a from s using policy derived from Q
		a = settings.policy_fn(q_table[s])
		terminate_ep = False
		while terminate_ep == False:
			# Take action a, observe r, s'
			r, s_prime, terminate_ep = env.step(a)
			# Choose a' from s' using policy derived from Q
			a_prime = settings.policy_fn(q_table[s_prime])
			q_table[s, a] += settings.alpha * (r + settings.gamma * q_table[s_prime, a_prime] - q_table[s, a])
			s = s_prime
			a = a_prime

			stats.update(ep, s, r)

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return q_table, stats


# Q-learning with ET (accumulating traces, replacing traces)
class SettingsQlearningEt:
	TRACE_TYPE_ACCUMULATING = 0
	TRACE_TYPE_REPLACING = 1

	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.lamb = 0.9
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)
		self.trace_type = self.TRACE_TYPE_ACCUMULATING

def train_q_learning_et(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))
	e_table = np.zeros((env.state_count, env.action_count))

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		a = np.random.choice(env.action_count)
		terminate_ep = False
		while terminate_ep == False:
			# Take action a, observe r, s'
			r, s_prime, terminate_ep = env.step(a)
			# Choose a' from s' using policy derived from Q
			a_prime = settings.policy_fn(q_table[s_prime])
			a_max = np.argmax(q_table[s_prime])
			delta = r + settings.gamma * q_table[s_prime, a_max] - q_table[s, a]
			e_table[s, a] += 1

			for (s, a) in np.ndindex((env.state_count, env.action_count)):
				q_table[s, a] += settings.alpha * delta * e_table[s, a]
				if a_prime == a_max:
					e_table[s, a] = settings.gamma * settings.lamb * e_table[s, a]
				else:
					e_table[s, a] = 0

			s = s_prime
			a = a_prime

			stats.update(ep, s, r)

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return q_table, stats


# Dyna-Q
class SettingsDynaQ:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.N = 50
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)

def train_dyna_q(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))
	model_s = np.zeros((env.state_count, env.action_count), dtype=int)
	model_r = np.zeros((env.state_count, env.action_count), dtype=float)
	observed = {}

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		terminate_ep = False
		while terminate_ep == False:
			a = settings.policy_fn(q_table[s])
			if s not in observed:
				observed[s] = set()
			observed[s].add(a)
			r, s_prime, terminate_ep = env.step(a)
			model_s[s, a] = s_prime
			model_r[s, a] = r

			q_table[s, a] += settings.alpha * (r + settings.gamma * np.max(q_table[s_prime]) - q_table[s, a])

			stats.update(ep, s_prime, r)

			s_next = s_prime
			i = 0
			while i < settings.N:
				s = np.random.choice(list(observed.keys()))
				a = np.random.choice(tuple(observed[s]))
				s_prime = model_s[s, a]
				r 		= model_r[s, a]
				q_table[s, a] += settings.alpha * (r + settings.gamma * np.max(q_table[s_prime]) - q_table[s, a])

				i += 1
			s = s_next

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count


	return q_table, stats


# Prioritized Sweeping
class SettingsPrioritizedSweeping:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.theta = 1e-6
		self.N = 50
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)

class QueuePrioritizedSweeping:
	def __init__(self):
		self.h = []

	def enqueue(self, p, sa):
		for i, [p_old, sa_old] in enumerate(self.h):
			if sa_old == sa:
				self.h[i][0] = np.min([p_old, p])
				heapq.heapify(self.h)
				return

		heapq.heappush(self.h, [p, sa])

	def dequeue(self):
		[_, sa] = heapq.heappop(self.h)
		return sa

	def empty(self):
		return len(self.h) == 0

def train_prioritized_sweeping(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)
	q_table = np.ones((env.state_count, env.action_count))
	model_s = np.zeros((env.state_count, env.action_count), dtype=int)
	model_r = np.zeros((env.state_count, env.action_count), dtype=float)
	q = QueuePrioritizedSweeping()
	experienced = []

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		terminate_ep = False
		while terminate_ep == False:
			a = settings.policy_fn(q_table[s])
			r, s_prime, terminate_ep = env.step(a)
			if not ((s, a) in experienced):
				model_s[s, a] = s_prime
				model_r[s, a] = r
				experienced.append((s, a))
			p = np.abs(r + settings.gamma * np.max(q_table[s_prime]) - q_table[s, a])
			if p > settings.theta:
				q.enqueue(-p, (s, a))

			stats.update(ep, s_prime, r)

			s_next = s_prime
			i = 0
			while (i < settings.N) and (not q.empty()):
				(s, a) = q.dequeue()
				s_prime = model_s[s, a]
				r 		= model_r[s, a]
				q_table[s, a] += settings.alpha * (r + settings.gamma * np.max(q_table[s_prime]) - q_table[s, a])

				for (s_predicted, a_predicted) in experienced:
					if s == model_s[s_predicted, a_predicted]:
						r_predicted = model_r[s_predicted, a_predicted]
						p = np.abs(r_predicted + settings.gamma * np.max(q_table[s]) - q_table[s_predicted, a_predicted])
						if p > settings.theta:
							q.enqueue(-p, (s_predicted, a_predicted))

				i += 1
			s = s_next

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return q_table, stats


# Q-learning with function approximation
class SettingsQLearningFa:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)
		self.approximator_fn = None

def train_q_learning_fa(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		terminate_ep = False
		while terminate_ep == False:
			a = settings.policy_fn(settings.approximator_fn(env, s))
			r, s_prime, terminate_ep = env.step(a)
			delta = r + settings.gamma * np.max(settings.approximator_fn(env, s_prime)) - settings.approximator_fn(env, s, a)

			for i in range(settings.approximator_fn.feature_count):
				settings.approximator_fn.weights[i] += settings.alpha * delta * settings.approximator_fn.feature_fns[i](env, s, a)

			s = s_prime

			stats.update(ep, s, r)

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return settings.approximator_fn, stats


# SARSA with function approximation
class SettingsSarsaFa:
	def __init__(self):
		self.max_ep_count = 1000
		self.alpha = 0.1
		self.gamma = 0.1
		self.policy_fn = PolicyEGreedyFn(epsilon=0.25)
		self.approximator_fn = None

def train_sarsa_fa(env, settings):
	stats = Statistics(settings.max_ep_count, env.state_count)

	for ep in pyprind.prog_bar(range(settings.max_ep_count)):
		env.reset()
		s = env.get_start_state()
		a = settings.policy_fn(settings.approximator_fn(env, s))
		terminate_ep = False
		while terminate_ep == False:
			r, s_prime, terminate_ep = env.step(a)
			a_prime = settings.policy_fn(settings.approximator_fn(env, s))
			delta = r + settings.gamma * settings.approximator_fn(env, s_prime, a_prime) - settings.approximator_fn(env, s, a)
			for i in range(settings.approximator_fn.feature_count):
				settings.approximator_fn.weights[i] += settings.alpha * delta * settings.approximator_fn.feature_fns[i](env, s, a)

			s = s_prime
			a = a_prime

			stats.update(ep, s, r)

		if settings.policy_fn.epsilon > 0.05:
			settings.policy_fn.epsilon -= settings.policy_fn.epsilon_initial / settings.max_ep_count

	return settings.approximator_fn, stats
